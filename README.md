# Micropython C Generator

A single-page site for generating C code for Micropython function implementation.
Hosted at https://mpy-c-gen.oliverrobson.tech/.
